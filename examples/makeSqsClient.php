<?php
/**
 * This is an example on how to correctly instantiate a SQS queue client together with all dependencies that you should
 * set up on whatever service manager apparatus your application uses.
 */

use Aws\Sqs\SqsClient;
use BBCStore\Catalogue\Queue\Client\AWS\Sqs;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\SerializerFactory;

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * First of all, bootstrap dependencies - this example instantiates a SQS implementation of our
 * Queueing\ClientInterface; it needs a message serializer as well as an AWS SDK sqs client.
 *
 * The message serializer in turn uses Symfony's serializer.
 */

// Get us first the AWS SDK SQS client, preconfigured with actual AWS credentials.
$awsCredentials = include __DIR__ . '/awsCredentials.php';
$awsSqsClient   = SqsClient::factory($awsCredentials);

// Secondly, we need the message serializer
$messageSerializer = SerializerFactory::getInstance();

// Finally, instantiate our queue client
$client = new Sqs($awsSqsClient, $messageSerializer);
$client->subscribe($awsCredentials['sqsQueueName']);
return $client;
