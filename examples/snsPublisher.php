<?php
/**
 * This is an example on how to correctly instantiate a SNS queue client together with all dependencies that you should
 * set up on whatever service manager apparatus your application uses.
 */

use Aws\Sns\SnsClient;
use BBCStore\Catalogue\Queue\Client\AWS\Sns;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\SerializerFactory;

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * First of all, bootstrap dependencies - this example instantiates a SQS implementation of our
 * Queueing\ClientInterface; it needs a message serializer as well as an AWS SDK sqs client.
 *
 * The message serializer in turn uses Symfony's serializer.
 */

// Get us first the AWS SDK SQS client, preconfigured with actual AWS credentials.
$awsCredentials = include __DIR__ . '/awsCredentials.php';
$awsSnsClient   = SnsClient::factory($awsCredentials);

// Secondly, we need the message serializer
$messageSerializer = SerializerFactory::getInstance();

// Finally, instantiate our queue client
$client    = new Sns($awsSnsClient, $messageSerializer);
$client->subscribe($awsCredentials['snsArn']);

// Create a message
$message = new Message();
$message
    ->setResourceId((string) random_int(0, 100000))
    ->setResourceType((string) random_int(0, 100000))
    ->setOperationType(Message::OPERATION_CREATE);

// ... and publish! The return value is an updated message updated with the message ID
$message = $client->publish($message);

echo "\n Message sent:\n";
dump($message);
