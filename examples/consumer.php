<?php
/**
 * This is an example on how to implement a queue consumer.
 */

require_once __DIR__ . '/../vendor/autoload.php';

// Finally, instantiate our queue client
$queue = include __DIR__ . '/makeSqsClient.php';

do {
    // Acquire message
    $message = $queue->next();

    // Do something with it
    if ($message !== false) {
        dump($message);

        // All good? notify queue message was processed
        $queue->succeed($message);
    } else {
        dump('No available messages on the queue...');
    }

    // Sleep a little
    sleep(1);
} while (true);
