<?php
/**
 * This is an example on how to implement a queue publisher.
 */

use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message;

require_once __DIR__ . '/../vendor/autoload.php';

// Finally, instantiate our queue client
$queue = include __DIR__ . '/makeSqsClient.php';

// Create a message
$message = new Message();
$message
    ->setResourceId((string) random_int(0, 100000))
    ->setResourceType((string) random_int(0, 100000))
    ->setOperationType(Message::OPERATION_CREATE);

// ... and publish! The return value is an updated message updated with the message ID
$message = $queue->publish($message);

echo "\n Message sent:\n";
dump($message);
