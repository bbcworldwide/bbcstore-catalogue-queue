<?php
namespace BBCStore\Catalogue\Queue\Tests;

use Symfony\Component\VarDumper\VarDumper;

/**
 * Base class for tests with a number of common data providers.
 *
 * @author BBC Worldwide
 */
class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Wrapper around symfony's var dumper
     *
     * @param array ...$args
     */
    public static function dump(...$args)
    {
        foreach ($args as $var) {
            VarDumper::dump($var);
        }
    }


    /***** Data providers *****/

    /**
     * Returns a list random strings.
     *
     * @return array
     */
    public function randomStringsDataProvider()
    {
        return [
            ['foo'],
            [uniqid(random_int(0, 1000000), true)],
            [(string) random_int(0, 10000)],
        ];
    }

    /**
     * A simple boolean data provider.
     *
     * @return array
     */
    public function booleanDataProvider()
    {
        return [
            [true],
            [false],
        ];
    }
}
