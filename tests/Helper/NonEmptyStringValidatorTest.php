<?php
namespace BBCStore\Catalogue\Tests\Helper;

use BBCStore\Catalogue\Queue\Helper\NonEmptyStringValidator;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

class NonEmptyStringValidatorTest extends AbstractTestCase
{
    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Helper\Exception\ValidationError
     * @expectedExceptionMessage Value is not a string
     * @dataProvider             notStringsDataProvider
     */
    public function validateThrowsExceptionWithNonStrings($notAString)
    {
        NonEmptyStringValidator::validate($notAString);
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Helper\Exception\ValidationError
     * @expectedExceptionMessage Value is null
     */
    public function validateThrowsExceptionWithNullNonStrings()
    {
        NonEmptyStringValidator::validate(null);
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Helper\Exception\ValidationError
     * @expectedExceptionMessage Value is empty
     * @dataProvider             emptyStringsDataProvider
     */
    public function validateThrowsExceptionWithEmptyStrings($emptyString)
    {
        NonEmptyStringValidator::validate($emptyString);
    }

    /**
     * @test
     * @dataProvider validStringsDataProvider
     */
    public function validateDoesntThrowExceptionWithValidStrings($validString)
    {
        try {
            NonEmptyStringValidator::validate($validString);
        } catch (\Exception $ex) {
            self::fail(sprintf('Exception thrown: %s', get_class($ex)));
        }
    }

    public function notStringsDataProvider()
    {
        return [
            [5],
            [[]],
            [new \DateTime()],
        ];
    }

    public function emptyStringsDataProvider()
    {
        return [
            [''],
            ["\t"],
            ["\n"],
        ];
    }

    public function validStringsDataProvider()
    {
        return [
            ['foo'],
            ['bar'],
            ['546546543513165'],
        ];
    }
}
