<?php
namespace BBCStore\Catalogue\Reader\Tests\Helper;

use BBCStore\Catalogue\Queue\Helper\CoalesceTrait;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

class CoalesceTraitTestClass
{
    use CoalesceTrait;

    /**
     * Passthrough method to coalesce.
     *
     * @param mixed      $structure
     * @param string     $key
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function coalescePassthrough($structure, $key, $default = null)
    {
        return $this->coalesce($structure, $key, $default);
    }

}

class CoalesceTraitTest extends AbstractTestCase
{
    /**
     * @var CoalesceTraitTestClass
     */
    private $instance;

    public function setUp()
    {
        $this->instance = new CoalesceTraitTestClass();
    }

    /**
     * @test
     * @dataProvider             invalidKeysDataProvider
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid key
     */
    public function coalesceThrowsExceptionWithDodgyKeyName($dodgyKey)
    {
        $this->instance->coalescePassthrough(new \stdClass(), $dodgyKey, 'bar');
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @dataProvider                   invalidStructuresDataProvider
     * @expectedExceptionMessageRegExp /must be either an array or an instance/
     */
    public function coalesceThrowsExceptionWithInvalidStructures($invalidStructure)
    {
        $this->instance->coalescePassthrough($invalidStructure, 'foo');
    }

    /**
     * @test
     * @dataProvider validStructuresWithPropertyNamesAndValuesInThatOrderBro
     */
    public function coalesceRetrievesDataWhenItExists($structure, $propertyName, $defaultValue)
    {
        self::assertSame($defaultValue, $this->instance->coalescePassthrough($structure, $propertyName));
    }

    /**
     * @test
     * @dataProvider validStructuresWithPropertyNamesAndValuesInThatOrderBro
     */
    public function coalesceCoalescesToNullWhenKeyDoesntExist($structure)
    {
        $property = (string) random_int(100, 200);
        self::assertNull($this->instance->coalescePassthrough($structure, $property));
    }

    /**
     * @test
     * @dataProvider validStructuresWithPropertyNamesAndValuesInThatOrderBro
     */
    public function coalesceCoalescesToDefaultValueWhenKeyDoesntExist($structure)
    {
        $defaultValue = (string) random_int(8000, 10000);
        $property     = (string) random_int(100, 200);
        self::assertSame($defaultValue, $this->instance->coalescePassthrough($structure, $property, $defaultValue));
    }

    /***** Data providers *****/

    /**
     * @return array
     */
    public function invalidStructuresDataProvider()
    {
        return [
            [new \DateTime()],
            ['foo'],
            [5],
        ];
    }

    /**
     * @return array
     */
    public function validStructuresWithPropertyNamesAndValuesInThatOrderBro()
    {
        $stdClass      = new \stdClass();
        $stdClass->bar = 'foo';

        $date = new \DateTime();

        return [
            [
                ['foo' => 'bar'],
                'foo',
                'bar',
            ],
            [
                $stdClass,
                'bar',
                'foo',
            ],
            [
                ['date' => $date],
                'date',
                $date,
            ],
        ];
    }

    /**
     * @return array
     */
    public function invalidKeysDataProvider()
    {
        return [
            [[]],
            [new \stdClass()],
            [' '],
            ["\t"],
            ["  \t\n"],
        ];
    }
}
