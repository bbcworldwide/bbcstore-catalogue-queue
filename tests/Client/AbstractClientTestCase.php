<?php
namespace BBCStore\Catalogue\Queue\Tests\Client;

use BBCStore\Catalogue\Queue\Message;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

/**
 * Base class for client tests
 *
 * @author BBC Worldwide
 */
abstract class AbstractClientTestCase extends AbstractTestCase
{
    const MOCK_QUEUE_NAME = 'foobar-queue';

    /**
     * @var \BBCStore\Catalogue\Queue\Message\PCSUpdates\Serializer|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $serializer;

    public function setUp()
    {
        parent::setUp();

        $this->serializer = $this
            ->getMockBuilder(Message\SerializerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->serializer = null;
    }

    /**
     * Returns a message which can self validate
     *
     * @return \BBCStore\Catalogue\Queue\Message\PCSUpdates\Message
     */
    protected function getValidMessage()
    {
        $message = new Message\PCSUpdates\Message();
        $message
            ->setResourceId('foo')
            ->setResourceType('bar')
            ->setOperationType(Message\PCSUpdates\Message::OPERATION_CREATE)
            ->setOriginalCorrelationId('582c6abcf271e8.22107147');

        return $message;
    }

    /**
     * Returns a message which can self validate
     *
     * @return array
     */
    public function validMessagesDataProvider()
    {
        $messageA = new Message\PCSUpdates\Message();
        $messageA
            ->setResourceId('foo')
            ->setResourceType('bar')
            ->setOperationType(Message\PCSUpdates\Message::OPERATION_CREATE)
            ->setOriginalCorrelationId('582c6abcf271e8.22107147');

        $sqsMessageAttributesA = [];

        $messageB = new Message\B2BPusherUpdates\Message();
        $messageB
            ->setXml('xml galore')
            ->setTrackingId('foo')
            ->setCorrelationId('bar')
            ->setChangeEventOndemandPid('foobar')
            ->addError('foo', 'bar');

        $sqsMessageAttributesB = [
            'BBC.TrackingId'             => [
                'DataType'    => 'String',
                'StringValue' => 'foo',
            ],
            'BBC.CorrelationId'          => [
                'DataType'    => 'String',
                'StringValue' => 'bar',
            ],
            'BBC.ChangeEventOndemandPid' => [
                'DataType'    => 'String',
                'StringValue' => 'foobar',
            ],
            'processing-errors.foo'      => [
                'DataType'    => 'String',
                'StringValue' => 'bar',
            ],
        ];

        return [
            [
                $messageA,
                $sqsMessageAttributesA,
            ],
            [
                $messageB,
                $sqsMessageAttributesB,
            ],
        ];
    }

}
