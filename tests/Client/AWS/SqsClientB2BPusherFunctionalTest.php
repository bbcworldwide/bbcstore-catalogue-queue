<?php
namespace BBCStore\Catalogue\Queue\Tests\Client\AWS;

use Aws\Sqs\SqsClient;
use BBCStore\Catalogue\Queue\Client\AWS\Sqs;
use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Message;
use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Serializer;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;
use Guzzle\Service\Resource\Model;

class SqsClientB2BPusherFunctionalTest extends AbstractTestCase
{
    /**
     * Test parsing a b2bpusher payload into a message.
     *
     * @test
     */
    public function functionalNextAndPublish()
    {
        // Mock to AWS SDK SQS client
        $awsSqsClient = $this->getMockBuilder(SqsClient::class)->disableOriginalConstructor()->getMock();

        // Make mock data
        $body          = 'XML body';
        $messageId     = 'lerele';
        $receiptHandle = 'pesky receipt handle';
        $correlationId = 'correlation-id';
        $trackingId    = 'tracking-id';
        $changePid     = 'changePid';

        // AWS payload
        $mockPayload = [
            [
                'Body'              => $body,
                'MessageId'         => $messageId,
                'ReceiptHandle'     => $receiptHandle,
                'MessageAttributes' => [
                    'BBC.CorrelationId'          => [
                        'DataType'    => 'String',
                        'StringValue' => $correlationId,
                    ],
                    'BBC.TrackingId'             => [
                        'DataType'    => 'String',
                        'StringValue' => $trackingId,
                    ],
                    'BBC.ChangeEventOndemandPid' => [
                        'DataType'    => 'String',
                        'StringValue' => $changePid,
                    ],
                ],
            ],
        ];

        // Message we'll be sending back and forth
        $message = new Message();
        $message
            ->setXml($body)
            ->setCorrelationId($correlationId)
            ->setChangeEventOndemandPid($changePid)
            ->setTrackingId($trackingId)
            ->setMessageId($messageId)
            ->addMetadata('receiptHandle', $receiptHandle);

        // Make instance to tested class
        $queue = new Sqs($awsSqsClient, new Serializer());
        $queue->subscribe('foo');

        // First call is always to get the queue URL
        $urlModel = new Model();
        $urlModel->set('QueueUrl', 'http://bar');

        $awsSqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willReturn($urlModel);

        // Second call is to receive message
        $payloadModel = new Model();
        $payloadModel->set('Messages', $mockPayload);

        $awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willReturn($payloadModel);

        // Third call is to send a message
        $awsSqsClient
            ->expects(self::at(2))
            ->method('__call')
            ->with('sendMessage')
            ->willReturn($payloadModel);

        // Test get next
        self::assertEquals($message, $queue->next());

        // Test publish
        self::assertSame($message, $queue->publish($message));
    }
}
