<?php

namespace BBCStore\Catalogue\Queue\Tests\Client\AWS;

use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use BBCStore\Catalogue\Queue\Client\AWS\Sns;
use BBCStore\Catalogue\Queue\Exception\PublishException;
use BBCStore\Catalogue\Queue\Message\MessageInterface;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message;
use BBCStore\Catalogue\Queue\Tests\Client\AbstractClientTestCase;
use Guzzle\Service\Resource\Model;

class SnsClientTest extends AbstractClientTestCase
{
    /**
     * @var SnsClient|\PHPUnit_Framework_MockObject_MockObject
     */
    private $snsClient;

    /**
     * @var Sns
     */
    private $instance;

    const MOCK_QUEUE_NAME = 'arn:aws:sns:local:000000000000:local-topic1';

    public function setUp()
    {
        parent::setUp();

        $this->snsClient = $this->getMockBuilder(SnsClient::class)->disableOriginalConstructor()->getMock();

        $this->instance = new Sns($this->snsClient, $this->serializer);
        $this->instance->subscribe(self::MOCK_QUEUE_NAME);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->snsClient  = null;
        $this->serializer = null;
        $this->instance   = null;
    }

    /***** Reading from the queue is not supported on this driver, we can only publish *****/

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\UnsupportedOperationException
     */
    public function nextThrowsUnsupportedOperationException()
    {
        $this->instance->next();
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\UnsupportedOperationException
     */
    public function succeedThrowsUnsupportedOperationException()
    {
        $this->instance->succeed(new Message());
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\QueueingException
     * @expectedExceptionMessage Queue name not configured
     */
    public function failWithNoConfiguredQueue()
    {
        $message = new Message();
        $message
            ->setResourceId('foo')
            ->setResourceType('bar')
            ->setOperationType('create')
            ->setOriginalCorrelationId('582c6abcf271e8.22107147');

        $queue = new Sns($this->snsClient, $this->serializer);
        $queue->publish($message);
    }

    /**
     * @test
     * @dataProvider             randomStringsDataProvider
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage queueName must be an AWS arn
     */
    public function publishFailsOnDodgyQueueName($notAnArn)
    {
        $this->instance
            ->subscribe($notAnArn)
            ->publish($this->getValidMessage());
    }

    /**
     * @test
     */
    public function publishHandlesSqsException()
    {
        $exceptionMessage = 'asasdasdbarfoo';
        $exception        = new SnsException($exceptionMessage);

        $this->expectException(PublishException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish')
            ->willThrowException($exception);

        $this->instance->publish($this->getValidMessage());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishSucceeds(MessageInterface $message, array $messageAttributes)
    {
        $serializedMessage = 'foobarfoo';
        $messageId         = 'asdasd';

        $messagePayload = [
            'TopicArn'          => self::MOCK_QUEUE_NAME,
            'Message'           => $serializedMessage,
            'MessageAttributes' => $messageAttributes,
        ];

        $resultModel = new Model();
        $resultModel->set('MessageId', $messageId);

        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn($serializedMessage);

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish', [$messagePayload])
            ->willReturn($resultModel);

        // Checking it's the same instance and type
        self::assertSame($message, $this->instance->publish($message));

        // Also check messageId is there
        self::assertSame($messageId, $message->getMessageId());
    }
}
