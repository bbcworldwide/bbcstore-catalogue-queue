<?php
namespace BBCStore\Catalogue\Tests\Queue\Client\AWS;

use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use BBCStore\Catalogue\Queue\Client\AWS\Sqs;
use BBCStore\Catalogue\Queue\Exception\PublishException;
use BBCStore\Catalogue\Queue\Exception\PurgeException;
use BBCStore\Catalogue\Queue\Exception\QueueingException;
use BBCStore\Catalogue\Queue\Message\MessageInterface;
use BBCStore\Catalogue\Queue\Tests\Client\AbstractClientTestCase;
use Guzzle\Service\Resource\Model;

/**
 * IMPORTANT NOTE: AWS SDK client is evil as it uses magic methods via __call. There's a bug also on PHP7
 * whereby if you try to mock a particular __call to the client, and you don't specify exactly self::at(rightNumberHere)
 * you'll get a segfault. Doesn't happen on PHP5 though.
 *
 * @author BBC Worldwide
 */
class SqsClientTest extends AbstractClientTestCase
{
    /**
     * @var SqsClient|\PHPUnit_Framework_MockObject_MockObject
     */
    private $sqsClient;

    /**
     * @var Sqs
     */
    private $instance;

    const MOCK_QUEUE_URL = 'http://foo/bar';

    public function setUp()
    {
        parent::setUp();

        $this->sqsClient = $this->getMockBuilder(SqsClient::class)->disableOriginalConstructor()->getMock();

        $this->instance = new Sqs($this->sqsClient, $this->serializer);
        $this->instance->subscribe(self::MOCK_QUEUE_NAME);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->sqsClient = null;
        $this->instance  = null;
    }

    protected function setGetQueueUrlExpectation()
    {
        // First call is always to get the queue URL
        $urlModel = new Model();
        $urlModel->set('QueueUrl', self::MOCK_QUEUE_URL);

        $this->sqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willReturn($urlModel);
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\QueueingException
     * @expectedExceptionMessage Queue name not configured
     */
    public function failWithNoConfiguredQueue()
    {
        $queue = new Sqs($this->sqsClient, $this->serializer);
        $queue->next();
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\DeleteException
     */
    public function doSucceedThrowsDeleteException()
    {
        $this->setGetQueueUrlExpectation();

        $exception = new SqsException();
        $exception->setExceptionCode('AWS.SimpleQueueService.NonExistentQueue');

        $receiptHandle = 'foobar';
        $message       = $this->getValidMessage();
        $message->addMetadata('receiptHandle', $receiptHandle);

        $this->sqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willReturn(self::MOCK_QUEUE_URL);

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('deleteMessage')
            ->willThrowException($exception);

        // First call is always to get the queue URL
        $urlModel = new Model();
        $urlModel->set('QueueUrl', 'http://foo/bar');

        $this->instance->succeed($message);
    }

    /**
     * @test
     */
    public function doSucceedGetQueueUrlCreatesQueueIfNotExists()
    {
        $this->setGetQueueUrlExpectation();

        $exception = new SqsException();
        $exception->setExceptionCode('AWS.SimpleQueueService.NonExistentQueue');

        $receiptHandle = 'foobar';
        $message       = $this->getValidMessage();
        $message->addMetadata('receiptHandle', $receiptHandle);

        $sqsPayload = [
            'QueueUrl'      => self::MOCK_QUEUE_URL,
            'ReceiptHandle' => $receiptHandle,
        ];

        $this->sqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willThrowException($exception);

        // First call is always to get the queue URL
        $urlModel = new Model();
        $urlModel->set('QueueUrl', 'http://foo/bar');

        // This is it
        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('createQueue', [['QueueName' => self::MOCK_QUEUE_NAME]])
            ->willReturn($urlModel);

        // Don't care about the rest
        $this->sqsClient
            ->expects(self::at(2))
            ->method('__call')
            ->with('deleteMessage', [$sqsPayload]);

        $this->instance->succeed($message);
    }

    /**
     * @test
     */
    public function doSucceedGetQueueReconvertsExceptionOnUnhandledSqsError()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'barfoobar';
        $exception        = new SqsException($exceptionMessage);
        $exception->setExceptionCode('foo');

        $message = $this->getValidMessage();
        $message->addMetadata('receiptHandle', 'foo');

        $this->sqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willThrowException($exception);

        $this->expectException(QueueingException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->instance->succeed($message);
    }

    /**
     * @test
     * @expectedException \BBCStore\Catalogue\Queue\Exception\ReadException
     */
    public function nextFailsWithReadException()
    {
        $this->setGetQueueUrlExpectation();

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willThrowException(new SqsException());

        self::assertFalse($this->instance->next());
    }

    /**
     * @test
     */
    public function nextOnEmptyQueue()
    {
        $this->setGetQueueUrlExpectation();

        // Make SQS return nothing
        $sqsResult = new Model();

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willReturn($sqsResult);

        self::assertFalse($this->instance->next());
    }

    /**
     * @test
     */
    public function nextSucceeds()
    {
        $this->setGetQueueUrlExpectation();

        // Message mock data
        $messageId      = 'foobar';
        $receiptHandle  = 'barfoo';
        $body           = 'fakejson';
        $messagePayload = [
            [
                'Body'              => $body,
                'MessageId'         => $messageId,
                'ReceiptHandle'     => $receiptHandle,
                'MessageAttributes' => [
                    'foo' => [
                        'DataType'    => 'String',
                        'StringValue' => 'bar',
                    ],
                ],
            ],
        ];

        // Add message attribute from above
        $message = $this->getValidMessage();
        $message->addMetadata('foo', 'bar');

        // Make SQS return message
        $sqsResult = new Model();
        $sqsResult->set('Messages', $messagePayload);

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willReturn($sqsResult);

        $this->serializer
            ->expects(self::once())
            ->method('deserialize')
            ->with($body)
            ->willReturn($message);

        // Check we're getting the same message instance
        self::assertSame($message, $this->instance->next());

        // Check values set dynamically
        self::assertSame($messageId, $message->getMessageId());
        self::assertSame($receiptHandle, $message->getMetadata('receiptHandle'));
    }

    /**
     * @test
     */
    public function publishHandlesSqsException()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'asasdasdbarfoo';
        $exception        = new SqsException($exceptionMessage);

        $this->expectException(PublishException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage')
            ->willThrowException($exception);

        $this->instance->publish($this->getValidMessage());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishSucceeds(MessageInterface $message, array $messageAttributes)
    {
        $this->setGetQueueUrlExpectation();

        $serializedMessage = 'foobarfoo';
        $messageId         = 'asdasd';
        $messagePayload    = [
            'QueueUrl'          => self::MOCK_QUEUE_URL,
            'MessageBody'       => $serializedMessage,
            'MessageAttributes' => $messageAttributes,
        ];

        $resultModel = new Model();
        $resultModel->set('MessageId', $messageId);

        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn($serializedMessage);

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage', [$messagePayload])
            ->willReturn($resultModel);

        // Checking it's the same instance and type
        self::assertSame($message, $this->instance->publish($message));

        // Also check messageId is there
        self::assertSame($messageId, $message->getMessageId());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishRemovesReceiptHandle(MessageInterface $message, array $expectedMessageAttributes)
    {
        $this->setGetQueueUrlExpectation();

        $key = Sqs::RECEIPT_HANDLE_KEY;

        $resultModel = new Model();
        $resultModel->set('MessageId', 'bar');

        // Add bogus receipt handle to message, make sure it's in there
        $receiptHandle = 'lerele';
        $message->addMetadata($key, $receiptHandle);
        self::assertSame($receiptHandle, $message->getMetadata($key));

        // Now, intercept message and make sure receipt handle has been taken away
        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn('foobarfoo');

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage', self::callback(function (array $payload) use ($expectedMessageAttributes) {
                self::assertArrayHasKey('MessageAttributes', $payload[0]);
                $messageAttributes = $payload[0]['MessageAttributes'];

                self::assertEquals($expectedMessageAttributes, $messageAttributes);
                self::assertArrayNotHasKey('receiptHandle', $messageAttributes);

                return true;
            }))
            ->willReturn($resultModel);

        self::assertSame($message, $this->instance->publish($message));
    }

    /**
     * @test
     */
    public function purgeSucceeds()
    {
        $this->setGetQueueUrlExpectation();

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('purgeQueue');

        $this->instance->purge();
    }

    /**
     * @test
     */
    public function purgeHandlesSqsException()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'asasdasdbarfoo';
        $exception        = new SqsException($exceptionMessage);

        $this->expectException(PurgeException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->sqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('purgeQueue')
            ->willThrowException($exception);

        $this->instance->purge();
    }
}
