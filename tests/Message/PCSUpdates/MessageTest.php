<?php
namespace BBCStore\Catalogue\Queue\Tests\Message\PCSUpdates;

use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

class MessageTest extends AbstractTestCase
{
    /**
     * @var Message
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();

        $this->instance = new Message();
    }

    /**
     * @test
     * @dataProvider             randomStringsDataProvider
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Message operation not allowed
     */
    public function setOperationTypeThrowsInvalidArgumentExceptionOnUnsupportedOperation($dodgyOperation)
    {
        $this->instance->setOperationType($dodgyOperation);
    }

    /**
     * @test
     * @dataProvider validOperationTypes
     */
    public function setOperationTypeSucceeds($validOperation)
    {
        self::assertNotEquals($validOperation, $this->instance->getOperationType());
        self::assertSame($this->instance, $this->instance->setOperationType($validOperation));
        self::assertEquals($validOperation, $this->instance->getOperationType());
    }

    /**
     * @test
     */
    public function summary()
    {
        $expectedSummary = [
            'resourceId'            => 'foo',
            'resourceType'          => 'bar',
            'operationType'         => 'create',
            'originalCorrelationId' => '582c6abcf271e8.22107147',
            'queueMeta'             => [
                'barfoo'            => 'foobar',
                'foobarfoo'         => 'barfoobar',
            ],
        ];

        $this->instance
            ->setResourceId($expectedSummary['resourceId'])
            ->setResourceType($expectedSummary['resourceType'])
            ->setOperationType($expectedSummary['operationType'])
            ->setOriginalCorrelationId($expectedSummary['originalCorrelationId']);

        foreach ($expectedSummary['queueMeta'] as $key => $value) {
            $this->instance->addMetadata($key, $value);
        }

        self::assertEquals($expectedSummary, $this->instance->summary());
    }

    /**
     * @test
     */
    public function gettersAndSetters()
    {
        $expectedSummary = [
            'resourceId'    => 'foo',
            'resourceType'  => 'bar',
            'operationType' => 'create',
            'originalCorrelationId' => '582c6abcf271e8.22107147',
            'queueMeta'     => [
                'barfoo'    => 'foobar',
                'foobarfoo' => 'barfoobar',
            ],
        ];

        $this->instance
            ->setResourceId($expectedSummary['resourceId'])
            ->setResourceType($expectedSummary['resourceType'])
            ->setOperationType($expectedSummary['operationType'])
            ->setOriginalCorrelationId($expectedSummary['originalCorrelationId']);

        foreach ($expectedSummary['queueMeta'] as $key => $value) {
            $this->instance->addMetadata($key, $value);
            self::assertSame($value, $this->instance->getMetadata($key));
        }

        self::assertSame($expectedSummary['resourceId'], $this->instance->getResourceId());
        self::assertSame($expectedSummary['resourceType'], $this->instance->getResourceType());
        self::assertSame($expectedSummary['operationType'], $this->instance->getOperationType());
        self::assertSame($expectedSummary['originalCorrelationId'], $this->instance->getOriginalCorrelationId());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Resource ID cannot be empty
     */
    public function selfValidateFailOnEmptyResourceId()
    {
        $this->instance
            ->setResourceType('foo')
            ->setOperationType('update');

        $this->instance->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Resource type cannot be empty
     */
    public function selfValidateFailOnEmptyResourceType()
    {
        $this->instance
            ->setResourceId('foo')
            ->setOperationType('create');

        $this->instance->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Operation type cannot be empty
     */
    public function selfValidateFailOnEmptyOperationType()
    {
        $this->instance
            ->setResourceId('foo')
            ->setResourceType('bar');

        $this->instance->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Original correlation id cannot be empty
     */
    public function selfValidateFailOnEmptyOriginalCorrelationId()
    {
        $this->instance
            ->setResourceId('foo')
            ->setResourceType('bar')
            ->setOperationType('update');

        $this->instance->selfValidate();
    }

    public function validOperationTypes()
    {
        return [
            ['create'],
            ['update'],
            ['delete'],
        ];
    }
}
