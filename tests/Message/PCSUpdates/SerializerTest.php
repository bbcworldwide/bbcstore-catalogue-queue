<?php
namespace BBCStore\Catalogue\Tests\Queue\Message\PCSUpdates;

use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Message as UnsupportedMessage;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\Serializer;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializer;

class SerializerTest extends AbstractTestCase
{
    /**
     * @var SymfonySerializer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $symfonySerializer;

    /**
     * @var Serializer
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();

        $this->symfonySerializer = $this->getMockBuilder(SymfonySerializer::class)->getMock();
        $this->instance          = new Serializer($this->symfonySerializer);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function serializeThrowsExceptionWithUnsupportedMessage()
    {
        $this->instance->serialize(new UnsupportedMessage());
    }

    /**
     * @test
     */
    public function serializeSucceeds()
    {
        $message  = new Message();
        $mockJson = 'mockjson';

        // Ensure we're calling the symfony serializer correctly
        $this->symfonySerializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message, 'json')
            ->willReturn($mockJson);

        // Ensure we're returning its output verbatim
        self::assertSame($mockJson, $this->instance->serialize($message));
    }

    /**
     * @test
     */
    public function deserializeSucceeds()
    {
        $message  = new Message();
        $mockJson = 'mockjson';

        // Ensure we're calling the symfony serializer correctly
        $this->symfonySerializer
            ->expects(self::once())
            ->method('deserialize')
            ->with($mockJson, get_class($message), 'json')
            ->willReturn($message);

        // Ensure we're returning its output verbatim
        self::assertSame($message, $this->instance->deserialize($mockJson));
    }
}
