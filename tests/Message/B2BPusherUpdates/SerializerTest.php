<?php
namespace BBCStore\Catalogue\Queue\Tests\Message\B2BPusherUpdates;

use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Message;
use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Serializer;
use BBCStore\Catalogue\Queue\Message\PCSUpdates\Message as UnsupportedMessage;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

class SerializerTest extends AbstractTestCase
{
    /**
     * @var Serializer
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();

        $this->instance = new Serializer();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Message is not a B2B Pusher message
     */
    public function serializeThrowsExceptionWithUnsupportedMessage()
    {
        $this->instance->serialize(new UnsupportedMessage());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Message contains no XML
     */
    public function serializeThrowsExceptionWithEmptyMessage()
    {
        $this->instance->serialize(new Message());
    }

    /**
     * @test
     */
    public function serializeSucceeds()
    {
        $mockXml = 'mockXml';
        $message = new Message();
        $message->setXml($mockXml);

        // Ensure we're returning its output verbatim
        self::assertSame($mockXml, $this->instance->serialize($message));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Cannot deserialize a non string value
     * @dataProvider             invalidDeserializerValuesDataProvider
     */
    public function deserializeThrowsExceptionOnInvalidValue($invalidValue)
    {
        $this->instance->deserialize($invalidValue);
    }

    /**
     * @test
     */
    public function deserializeSucceeds()
    {
        $mockXml = 'mockXml';
        $message = new Message();
        $message->setXml($mockXml);

        self::assertEquals($message, $this->instance->deserialize($mockXml));
    }

    public function invalidDeserializerValuesDataProvider()
    {
        return [
            [[]],
            [new \stdClass()],
            [new Message()],
        ];
    }
}
