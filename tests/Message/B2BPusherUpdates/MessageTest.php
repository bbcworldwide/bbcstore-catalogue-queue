<?php
namespace BBCStore\Catalogue\Queue\Tests\Message\B2BPusherUpdates;

use BBCStore\Catalogue\Queue\Message\B2BPusherUpdates\Message;
use BBCStore\Catalogue\Queue\Tests\AbstractTestCase;

class MessageTest extends AbstractTestCase
{
    /**
     * @var Message
     */
    private $instance;

    public function setUp()
    {
        parent::setUp();

        $this->instance = new Message();
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->instance = null;
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage XML cannot be empty
     */
    public function selfValidateThrowsExceptionOnNoXml()
    {
        $this->instance
            ->setTrackingId('foo')
            ->setCorrelationId('bar');

        $this->instance->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Tracking ID cannot be empty
     */
    public function selfValidateThrowsExceptionOnNoTrackingId()
    {
        $this->instance
            ->setXml('foo')
            ->setCorrelationId('bar');

        $this->instance->selfValidate();
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Correlation ID cannot be empty
     */
    public function selfValidateThrowsExceptionOnNoCorrelation()
    {
        $this->instance
            ->setXml('foo')
            ->setTrackingId('bar');

        $this->instance->selfValidate();
    }

    /**
     * @test
     */
    public function selfValidateSucceedsWithTheBareMinimum()
    {
        $this->instance
            ->setXml('xml')
            ->setTrackingId('tracking')
            ->setCorrelationId('correlation');

        self::assertNull($this->instance->selfValidate());
    }

    /**
     * @test
     */
    public function selfValidateSucceedsWithWartsAndAll()
    {
        $this->instance
            ->setXml('xml')
            ->setTrackingId('tracking')
            ->setCorrelationId('correlation')
            ->addError('foo', new \Exception())
            ->addError('bar', ['foo'])
            ->addMetadata('meta', 'data')
            ->setChangeEventOndemandPid('change');

        self::assertNull($this->instance->selfValidate());
    }

    /**
     * @test
     */
    public function removeMetadata()
    {
        $name  = 'foo';
        $value = 'var';

        $this->instance->addMetadata($name, $value);
        self::assertTrue($this->instance->removeMetadata($name));
        self::assertFalse($this->instance->removeMetadata($name));
    }
}
