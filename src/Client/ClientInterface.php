<?php
namespace BBCStore\Catalogue\Queue\Client;

use BBCStore\Catalogue\Queue\Exception\DeleteException;
use BBCStore\Catalogue\Queue\Exception\PublishException;
use BBCStore\Catalogue\Queue\Exception\PurgeException;
use BBCStore\Catalogue\Queue\Exception\ReadException;
use BBCStore\Catalogue\Queue\Message\MessageInterface;

/**
 * Interface to our queue clients.
 *
 * Each instance of a client MUST be bound to a specific queue, hence why the interface contains no queue
 * names.
 *
 * @package BBCStore\Catalogue\Queue\Client
 */
interface ClientInterface
{
    /**
     * Allows subscribing to a specific queue by whichever identifier is valid on the specific implementation (eg queue
     * name, topic, etc...).
     *
     * @param string $queue
     *
     * @return self
     */
    public function subscribe($queue);

    /**
     * Gets the next available message off the queue.
     *
     * If no message found on queue, return false.
     *
     * @return MessageInterface|false
     *
     * @throws ReadException
     */
    public function next();

    /**
     * Once a message has been processed successfully, call this method for any cleanup tasks associated with this
     * message. For instance, some queue implementations might require the message to be explicitly deleted; that
     * should be implemented here.
     *
     * @param MessageInterface $message
     *
     * @return void
     *
     * @throws DeleteException
     */
    public function succeed(MessageInterface $message);

    /**
     * Publishes a message into a given queue. Returns the message id, hydrated with message id,
     * or throws Exception\PublishException if anything goes wrong.
     *
     * @param MessageInterface $message
     *
     * @return MessageInterface
     *
     * @throws PublishException
     */
    public function publish(MessageInterface $message);

    /**
     * Purges all messages from the queue or throws Exception\PurgeException if anything goes wrong.
     *
     * @return self
     *
     * @throws PurgeException
     */
    public function purge();
}
