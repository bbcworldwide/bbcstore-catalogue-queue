<?php
namespace BBCStore\Catalogue\Queue\Client\AWS;

use BBCStore\Catalogue\Queue\Client\AbstractClient;
use BBCStore\Catalogue\Queue\Message\MessageInterface as Message;

/**
 * Common code to handling SQS and SNS.
 *
 * @author BBC Worldwide
 */
abstract class AbstractSqsSnsClient extends AbstractClient
{
    /**
     * Build SQS/SNS message attributes array off the message metadata.
     *
     * @param Message $message
     *
     * @return array
     */
    protected function makeMessageAttributes(Message $message)
    {
        // Compose MessageAttributes as per SQS structure
        $messageAttributes = [];
        foreach ($message->getAllMetadata() as $name => $value) {
            $messageAttributes[$name] = [
                'DataType'    => 'String',
                'StringValue' => $value,
            ];
        }

        return $messageAttributes;
    }
}
