<?php

namespace BBCStore\Catalogue\Queue\Client\AWS;

use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use BBCStore\Catalogue\Queue\Exception;
use BBCStore\Catalogue\Queue\Helper\CoalesceTrait;
use BBCStore\Catalogue\Queue\Helper\Exception\ValidationError;
use BBCStore\Catalogue\Queue\Message\MessageInterface as Message;
use BBCStore\Catalogue\Queue\Message\SerializerInterface as Serializer;

/**
 * Queue implementation using AWS SQS.
 *
 * @author BBC Worldwide
 * @see    http://docs.aws.amazon.com/aws-sdk-php/v2/guide/service-sqs.html
 */
class Sqs extends AbstractSqsSnsClient
{
    use CoalesceTrait;

    /**
     * Time in seconds before driver returns an empty response.
     * 20 seconds is the max amount AWS tolerates.
     */
    const LONG_POLLING_PERIOD = 20;

    /**
     * @var SqsClient
     */
    private $sqsClient;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $queueUrl;

    /**
     * Error code thrown on a SqsException when we're trying to fetch a queue that doesn't exist.
     */
    const SQS_ERROR_CODE_NON_EXISTENT_QUEUE = 'AWS.SimpleQueueService.NonExistentQueue';

    /**
     * Metadata key to use with SQS receipt handles.
     */
    const RECEIPT_HANDLE_KEY = 'receiptHandle';

    /**
     * Ensure a AWS SDK SQS client is passed in here. It must already be provisioned with AWS credentials and
     * generally ready to use.
     *
     * @param SqsClient  $sqsClient
     * @param Serializer $serializer
     */
    public function __construct(SqsClient $sqsClient, Serializer $serializer)
    {
        $this->sqsClient  = $sqsClient;
        $this->serializer = $serializer;
    }

    /**
     * SQS implementation of getNext.
     *
     * @inheritdoc
     * @throws ValidationError
     */
    protected function doGetNext()
    {
        try {
            $result = $this->sqsClient->receiveMessage([
                'QueueUrl'              => $this->getQueueUrl(),
                'MessageAttributeNames' => ['All'],
                'WaitTimeSeconds'       => self::LONG_POLLING_PERIOD,
            ]);
        } catch (SqsException $ex) {
            throw new Exception\ReadException($ex->getMessage(), $ex->getCode(), $ex);
        }

        $messages = $result->get('Messages');

        if ($messages === null) {
            throw new Exception\EmptyQueueException();
        }

        $message = $this->serializer->deserialize($messages[0]['Body']);
        $message
            ->setMessageId($messages[0]['MessageId'])
            ->addMetadata(self::RECEIPT_HANDLE_KEY, $messages[0]['ReceiptHandle']);

        // Message attributes, if any
        foreach ($this->coalesce($messages[0], 'MessageAttributes', []) as $attrName => $attrPayload) {
            $message->addMetadata($attrName, $this->coalesce($attrPayload, 'StringValue'));
        }

        return $message;
    }

    /**
     * SQS does not remove the message from the queue as we pick it up, instead it makes it unavailable for a time
     * to other consumers, and only gets rid of it if we tell it to do so. Which this method does
     *
     * @inheritdoc
     */
    protected function doSucceed(Message $message)
    {
        try {
            $this->sqsClient->deleteMessage([
                'QueueUrl'      => $this->getQueueUrl(),
                'ReceiptHandle' => $message->getMetadata(self::RECEIPT_HANDLE_KEY),
            ]);
        } catch (SqsException $ex) {
            throw new Exception\DeleteException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    /**
     * SQS implementation of publishing a message and necessary serialisation.
     *
     * @inheritdoc
     */
    protected function doPublish(Message $message)
    {
        try {
            // Remove receipt handle in those cases where we're getting a message bounced back into the queue
            $message->removeMetadata(self::RECEIPT_HANDLE_KEY);

            return $this->sqsClient->sendMessage([
                'QueueUrl'          => $this->getQueueUrl(),
                'MessageBody'       => $this->serializer->serialize($message),
                'MessageAttributes' => $this->makeMessageAttributes($message),
            ])->get('MessageId');
        } catch (SqsException $ex) {
            throw new Exception\PublishException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    /**
     * SQS implementation of purging queue.
     *
     * @inheritdoc
     */
    protected function doPurge()
    {
        try {
            $this->sqsClient->purgeQueue([
                'QueueUrl' => $this->getQueueUrl(),
            ]);
        } catch (SqsException $ex) {
            throw new Exception\PurgeException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    /**
     * Gets the queue URL, creates queue if it doesn't exist.
     *
     * @return mixed
     * @throws Exception\QueueingException
     */
    private function getQueueUrl()
    {
        if ($this->queueUrl === null) {
            $payload = ['QueueName' => $this->getSubscribedQueue()];

            try {
                $url = $this->sqsClient->getQueueUrl($payload)->get('QueueUrl');
            } catch (SqsException $ex) {
                switch ($ex->getAwsErrorCode()) {
                    case self::SQS_ERROR_CODE_NON_EXISTENT_QUEUE:
                        $result = $this->sqsClient->createQueue($payload);
                        $url    = $result->get('QueueUrl');
                        break;

                    default:
                        throw new Exception\QueueingException($ex->getMessage(), $ex->getCode(), $ex);
                }
            }

            $this->queueUrl = $url;
        }

        return $this->queueUrl;
    }
}
