<?php
namespace BBCStore\Catalogue\Queue\Client;

use BBCStore\Catalogue\Queue\Message\MessageInterface as Message;

/**
 * Null queue that doesn't do anything.
 *
 * @author BBC Worldwide
 * @codeCoverageIgnore
 */
class NullClient implements ClientInterface
{
    /**
     * @inheritdoc
     */
    public function next()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function succeed(Message $message)
    {
    }

    /**
     * @inheritdoc
     */
    public function publish(Message $message)
    {
        return $message;
    }

    /**
     * @inheritdoc
     */
    public function subscribe($queue)
    {
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function purge()
    {
        return $this;
    }
}
