<?php

namespace BBCStore\Catalogue\Queue\Message;

/**
 * Interface for queue messages.
 *
 * @package BBCStore\Catalogue\Queue\Message
 */
interface MessageInterface
{
    /**
     * Returns the message id - this will be whichever unique identifier applies to a single message on the
     * queue implementation.
     *
     * @return string
     */
    public function getMessageId();

    /**
     * Sets the message id.
     *
     * @param string $messageId
     *
     * @return MessageInterface
     */
    public function setMessageId($messageId);

    /**
     * Adds an entry to the metadata stack, by name.
     *
     * @param string $name
     * @param mixed  $value
     */
    public function addMetadata($name, $value);

    /**
     * Gets a specific value off the metadata stack. Returns null if it doesn't exist.
     *
     * @param string $name
     *
     * @return mixed|null
     */
    public function getMetadata($name);

    /**
     * Returns the whole metadata array.
     *
     * @return array
     */
    public function getAllMetadata();

    /**
     * Removes a given metadata entry, if it exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public function removeMetadata($name);

    /**
     * Ensure the message is consistent and ready to use.
     *
     * @throws \InvalidArgumentException
     */
    public function selfValidate();

    /**
     * Adds an error on to the metadata. Exceptions are formatted, and any other objects and arrays json encoded -
     * make sure they json encode correctly.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed
     */
    public function addError($name, $value);

    /**
     * Gets an array summary of the contents of the message.
     *
     * @return array
     */
    public function summary();
}
