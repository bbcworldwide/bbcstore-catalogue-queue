<?php

namespace BBCStore\Catalogue\Queue\Message\OLC;

use BBCStore\Catalogue\Queue\Message\OLC\Xml\Parser;
use BBCStore\Catalogue\Queue\Message\OLC\Xml\Sanitiser;

class MessageFactory
{
    public static function getInstance(string $xmlInput) : Message
    {
        $sanitiser = new Sanitiser();
        $xml = $sanitiser->sanitise($xmlInput);

        $parser = new Parser($xml);
        $id = $parser->getId();
        $type = $parser->getType();

        $message = new Message();
        $message
            ->setXml($xml)
            ->setEntityId($id)
            ->setEntityType($type);

        return $message;
    }
}
