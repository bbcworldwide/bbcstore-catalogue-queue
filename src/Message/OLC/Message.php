<?php
namespace BBCStore\Catalogue\Queue\Message\OLC;

use BBCStore\Catalogue\Queue\Message\AbstractMessage;
use BBCStore\Catalogue\Queue\Message\MessageInterface;

/**
 * Describes a queue message containing a feed item to be processed by the queue consumers.
 *
 * @package AppBundle\Queue
 * @author  BBC Worldwide
 */
class Message extends AbstractMessage implements MessageInterface
{
    /**
     * @var string
     */
    private $entityId;

    /**
     * @var string
     */
    private $entityType;

    /**
     * @var string
     */
    private $xml;

    /**
     * @var \DateTime
     */
    private $receivedAt;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var bool
     */
    private $forcedOverwrite = false;

    public function __construct()
    {
        $this->receivedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     * @return Message
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->entityType;
    }

    /**
     * @param string $entityType
     *
     * @return $this
     */
    public function setEntityType(string $entityType): self
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * @return string
     */
    public function getXml(): string
    {
        return $this->xml;
    }

    /**
     * @param string $xml
     *
     * @return $this
     */
    public function setXml(string $xml): self
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReceivedAt(): \DateTime
    {
        return $this->receivedAt;
    }

    /**
     * @param \DateTime|string $receivedAt
     * @return $this
     */
    public function setReceivedAt($receivedAt): self
    {
        if ($receivedAt && !$receivedAt instanceof \DateTime) {
            $this->receivedAt = new \DateTime($receivedAt);
        } else {
            $this->receivedAt = $receivedAt;
        }

        return $this;
    }

    /**
     * Fingerprints the message via MD5 of the XML.
     *
     * @return string
     */
    public function getHash(): string
    {
        if ($this->hash === null) {
            $this->hash = md5($this->getXml());
        }

        return $this->hash;
    }

    /**
     * @return boolean
     */
    public function isForcedOverwrite(): bool
    {
        return $this->forcedOverwrite;
    }

    /**
     * @param boolean $forcedOverwrite
     *
     * @return $this
     */
    public function setForcedOverwrite(bool $forcedOverwrite): self
    {
        $this->forcedOverwrite = $forcedOverwrite;

        return $this;
    }

    /**
     * Ensure the message is consistent and ready to use.
     *
     * @throws \InvalidArgumentException
     */
    public function selfValidate()
    {
        if ($this->xml === null) {
            throw new \InvalidArgumentException('XML cannot be null');
        }

        if ($this->entityId === null) {
            throw new \InvalidArgumentException('Entity Id cannot be null');
        }

        if ($this->entityType === null) {
            throw new \InvalidArgumentException('Entity Type cannot be null');
        }
    }

    /**
     * Gets an array summary of the contents of the message.
     *
     * @return array
     */
    public function summary()
    {
        return [
            'xml'               => $this->getXml(),
            'hash'              => $this->getHash(),
            'entityId'          => $this->getEntityId(),
            'entityType'        => $this->getEntityType(),
            'receivedAt'        => $this->getReceivedAt()->format(\DateTime::ATOM),
            'isForcedOverwrite' => $this->isForcedOverwrite(),
        ];
    }
}
