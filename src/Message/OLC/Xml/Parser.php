<?php

namespace BBCStore\Catalogue\Queue\Message\OLC\Xml;

/**
 * Biztalk XML entity parser.
 *
 * @author BBC Worldwide
 */
class Parser
{
    /**
     * @var \SimpleXMLElement
     */
    private $element;

    /**
     * EntityParser constructor.
     * @param string $xml
     *
     * @throws Exception\InvalidXmlException
     */
    public function __construct(string $xml)
    {
        try {
            $this->element = new \SimpleXMLElement($xml);
        } catch (\Exception $ex) {
            throw new Exception\InvalidXmlException($ex->getMessage());
        }
    }

    /**
     * Get entity ID from the 'code' element.
     *
     * @return string
     */
    public function getId(): string
    {
        return (string) $this->element->Code;
    }

    /**
     * Get entity type from the root element name.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->element->getName();
    }
}
