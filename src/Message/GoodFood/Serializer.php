<?php

namespace BBCStore\Catalogue\Queue\Message\GoodFood;

/**
 * GoodFood message serializer.
 *
 * Message for now is exactly the same as in PCS Updates, so serializer is still the same..
 *
 * @author BBC Worldwide
 */
class Serializer extends \BBCStore\Catalogue\Queue\Message\PCSUpdates\Serializer
{

}
