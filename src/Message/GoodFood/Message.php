<?php

namespace BBCStore\Catalogue\Queue\Message\GoodFood;

/**
 * GoodFood node updates message.
 *
 * Message for now is exactly the same as in PCS Updates, so simply extend from it.
 *
 * @author BBC Worldwide
 */
class Message extends \BBCStore\Catalogue\Queue\Message\PCSUpdates\Message
{
    /**
     * @var string
     */
    protected $resourceBundle;

    /**
     * @inheritdoc
     *
     * Override parent selfValidate as correlationId is not used in GF.
     */
    public function selfValidate()
    {
        if ($this->resourceId === null) {
            throw new \InvalidArgumentException('Resource ID cannot be empty');
        }

        if ($this->resourceType === null) {
            throw new \InvalidArgumentException('Resource type cannot be empty');
        }

        if ($this->operationType === null) {
            throw new \InvalidArgumentException('Operation type cannot be empty');
        }

        if (!empty($this->resourceBundle) && !is_string($this->resourceBundle)) {
            throw new \InvalidArgumentException('Resource bundle must be string');
        }
    }

    /**
     * @return string
     */
    public function getResourceBundle()
    {
        return $this->resourceBundle;
    }

    /**
     * @param string $resourceBundle
     *
     * @return Message
     */
    public function setResourceBundle($resourceBundle)
    {
        $this->resourceBundle = $resourceBundle;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function summary()
    {
        return [
            'resourceId'            => $this->resourceId,
            'resourceType'          => $this->resourceType,
            'resourceBundle'        => $this->resourceBundle,
            'operationType'         => $this->operationType,
            'queueMeta'             => $this->getAllMetadata(),
        ];
    }
}
