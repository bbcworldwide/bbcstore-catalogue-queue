<?php
namespace BBCStore\Catalogue\Queue\Message\B2BPusherUpdates;

use BBCStore\Catalogue\Queue\Message\MessageInterface;
use BBCStore\Catalogue\Queue\Message\SerializerInterface;

/**
 * Serializer used to encode messages to and fro the queue describing messages on the B2B Pusher events queue.
 *
 * @author BBC Worldwide
 */
class Serializer implements SerializerInterface
{
    /**
     * Serializes a message into our chosen format.
     *
     * @param Message|MessageInterface $message
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function serialize(MessageInterface $message)
    {
        if ($message instanceof Message === false) {
            throw new \InvalidArgumentException('Message is not a B2B Pusher message');
        } elseif ($message->getXml() === null) {
            throw new \InvalidArgumentException('Message contains no XML');
        }

        return $message->getXml();
    }

    /**
     * Deserializes an XML value into a message.
     *
     * @param string $value
     *
     * @return MessageInterface
     * @throws \InvalidArgumentException
     */
    public function deserialize($value)
    {
        if (is_string($value) === false) {
            throw new \InvalidArgumentException('Cannot deserialize a non string value');
        }

        $message = new Message();
        $message->setXml($value);

        return $message;
    }
}
