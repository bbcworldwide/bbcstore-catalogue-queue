<?php
namespace BBCStore\Catalogue\Queue\Message\B2BPusherUpdates;

use BBCStore\Catalogue\Queue\Message\AbstractMessage;
use BBCStore\Catalogue\Queue\Message\MessageInterface;

/**
 * Describes a B2B Pusher content update notification message.
 *
 * @author BBC Worldwide
 */
class Message extends AbstractMessage implements MessageInterface
{
    /**
     * Expected metadata attribute names.
     */
    const ATTR_CORRELATION_ID            = 'BBC.CorrelationId';
    const ATTR_TRACKING_ID               = 'BBC.TrackingId';
    const ATTR_CHANGE_EVENT_ONDEMAND_PID = 'BBC.ChangeEventOndemandPid';

    /**
     * Our addition
     */
    const ATTR_ERROR_PREFIX = 'BBC.ProcessingErrors.';

    /**
     * @var string.
     */
    private $xml;

    /**
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param string $xml
     *
     * @return self
     */
    public function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->getMetadata(self::ATTR_CORRELATION_ID);
    }

    /**
     * @param string $correlationId
     *
     * @return self
     */
    public function setCorrelationId($correlationId)
    {
        $this->addMetadata(self::ATTR_CORRELATION_ID, $correlationId);

        return $this;
    }

    /**
     * @return string
     */
    public function getTrackingId()
    {
        return $this->getMetadata(self::ATTR_TRACKING_ID);
    }

    /**
     * @param string $trackingId
     *
     * @return self
     */
    public function setTrackingId($trackingId)
    {
        $this->addMetadata(self::ATTR_TRACKING_ID, $trackingId);

        return $this;
    }

    /**
     * @return string
     */
    public function getChangeEventOndemandPid()
    {
        return $this->getMetadata(self::ATTR_CHANGE_EVENT_ONDEMAND_PID);
    }

    /**
     * @param string $changeEventOndemandPid
     *
     * @return self
     */
    public function setChangeEventOndemandPid($changeEventOndemandPid)
    {
        $this->addMetadata(self::ATTR_CHANGE_EVENT_ONDEMAND_PID, $changeEventOndemandPid);

        return $this;
    }

    /**
     * @inheritdoc
     *
     * @note: changeEventOndemandPid is optional.
     */
    public function selfValidate()
    {
        if ($this->xml === null) {
            throw new \InvalidArgumentException('XML cannot be empty');
        } elseif ($this->getCorrelationId() === null) {
            throw new \InvalidArgumentException('Correlation ID cannot be empty');
        } elseif ($this->getTrackingId() === null) {
            throw new \InvalidArgumentException('Tracking ID cannot be empty');
        }
    }

    /**
     * Gets an array summary of the contents of the message.
     *
     * @return array
     */
    public function summary()
    {
        return [
            'correlationId'          => $this->getCorrelationId(),
            'trackingId'             => $this->getTrackingId(),
            'changeEventOndemandPid' => $this->getChangeEventOndemandPid(),
            'xml'                    => $this->getXml(),
        ];
    }
}
