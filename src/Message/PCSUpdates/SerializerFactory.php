<?php
namespace BBCStore\Catalogue\Queue\Message\PCSUpdates;

use BBCStore\Catalogue\Queue\Message\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

/**
 * Makes a message serializer pre-configured with default values.
 *
 * @author BBC Worldwide
 * @codeCoverageIgnore
 */
class SerializerFactory
{
    /**
     * Returns a fully configured message serializer.
     *
     * @return SerializerInterface
     * @throws \InvalidArgumentException
     */
    public static function getInstance()
    {
        return new Serializer(new SymfonySerializer([new ObjectNormalizer()], [new JsonEncoder()]));
    }
}
