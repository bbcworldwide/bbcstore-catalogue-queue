<?php
namespace BBCStore\Catalogue\Queue\Message\PCSUpdates;

use BBCStore\Catalogue\Queue\Helper;
use BBCStore\Catalogue\Queue\Message\AbstractMessage;
use BBCStore\Catalogue\Queue\Message\MessageInterface;

/**
 * Describes a queue message containing a catalogue item of a certain type to to be processed by the queue consumers.
 *
 * $messageId should be populated with whatever message identifier is necessary on the queue service.
 *
 * @author  BBC Worldwide
 */
class Message extends AbstractMessage implements MessageInterface
{
    /**
     * Operations this message refers to
     */
    const OPERATION_CREATE = 'create';
    const OPERATION_UPDATE = 'update';
    const OPERATION_DELETE = 'delete';

    /**
     * @var array
     */
    private $allowedOperations = [
        self::OPERATION_CREATE,
        self::OPERATION_UPDATE,
        self::OPERATION_DELETE,
    ];

    /**
     * @var string
     */
    protected $resourceId;

    /**
     * @var string
     */
    protected $resourceType;

    /**
     * @var string
     */
    protected $operationType;

    /**
     * This is the correlation id from the orriginal request.
     * This allows for logs from applications that process the queue data to be traced back to the originating request.
     *
     * @var string
     */
    private $originalCorrelationId;

    /**
     * @inheritdoc
     */
    public function selfValidate()
    {
        if ($this->resourceId === null) {
            throw new \InvalidArgumentException('Resource ID cannot be empty');
        } elseif ($this->resourceType === null) {
            throw new \InvalidArgumentException('Resource type cannot be empty');
        } elseif ($this->operationType === null) {
            throw new \InvalidArgumentException('Operation type cannot be empty');
        } elseif ($this->originalCorrelationId === null) {
            throw new \InvalidArgumentException('Original correlation id cannot be empty');
        }
    }

    /**
     * @return array
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Anything goes. Just ensure payload serialises well.
     *
     * @param mixed $resourceId
     *
     * @return Message
     * @throws Helper\Exception\ValidationError
     */
    public function setResourceId($resourceId)
    {
        Helper\NonEmptyStringValidator::validate($resourceId);

        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }

    /**
     * @param string $resourceType
     *
     * @return Message
     * @throws Helper\Exception\ValidationError
     */
    public function setResourceType($resourceType)
    {
        Helper\NonEmptyStringValidator::validate($resourceType);

        $this->resourceType = $resourceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperationType()
    {
        return $this->operationType;
    }

    /**
     * @param string $operationType
     *
     * @return Message
     * @throws \InvalidArgumentException
     */
    public function setOperationType($operationType)
    {
        if (in_array($operationType, $this->allowedOperations, true) === false) {
            throw new \InvalidArgumentException(sprintf('Message operation not allowed (%s)', $operationType));
        }

        $this->operationType = $operationType;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalCorrelationId()
    {
        return $this->originalCorrelationId;
    }

    /**
     * @param string $originalCorrelationId
     *
     * @return Message
     * @throws Helper\Exception\ValidationError
     */
    public function setOriginalCorrelationId($originalCorrelationId)
    {
        Helper\NonEmptyStringValidator::validate($originalCorrelationId);

        $this->originalCorrelationId = $originalCorrelationId;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function summary()
    {
        return [
            'resourceId'            => $this->resourceId,
            'resourceType'          => $this->resourceType,
            'operationType'         => $this->operationType,
            'originalCorrelationId' => $this->originalCorrelationId,
            'queueMeta'             => $this->getAllMetadata(),
        ];
    }
}
