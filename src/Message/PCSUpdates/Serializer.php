<?php
namespace BBCStore\Catalogue\Queue\Message\PCSUpdates;

use BBCStore\Catalogue\Queue\Helper\Exception\ValidationError;
use BBCStore\Catalogue\Queue\Helper\NonEmptyStringValidator;
use BBCStore\Catalogue\Queue\Message\MessageInterface;
use BBCStore\Catalogue\Queue\Message\SerializerInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializer;

/**
 * Serializer used to encode messages to and fro the queue describing update events on the PCS.
 *
 * @author BBC Worldwide
 */
class Serializer implements SerializerInterface
{
    /**
     * Format we'll be serializing into
     */
    const SERIALIZE_TO = 'json';

    /**
     * @var SymfonySerializer
     */
    private $serializer;

    /**
     * Using a specific implementation (symfony) as to ensure any clients using this serializer speak the same
     * serialised format, otherwise decoding a serialised message on a different app could potentially break if the
     * serializer was different.
     *
     * @param SymfonySerializer $serializer
     */
    public function __construct(SymfonySerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Serializes a message into our chosen format.
     *
     * @param MessageInterface $message
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function serialize(MessageInterface $message)
    {
        $supportedClass = Message::class;

        if ($message instanceof $supportedClass === false) {
            throw new \InvalidArgumentException(sprintf(
                'Message must be an instance of %s, not %s',
                $supportedClass,
                get_class($message)
            ));
        }

        return $this->serializer->serialize($message, self::SERIALIZE_TO);
    }

    /**
     * Deserializes a json string into a Message.
     *
     * @param string $json
     *
     * @return MessageInterface
     * @throws ValidationError
     */
    public function deserialize($json)
    {
        NonEmptyStringValidator::validate($json);

        return $this->serializer->deserialize($json, Message::class, self::SERIALIZE_TO);
    }
}
