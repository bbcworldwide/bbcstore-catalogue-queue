<?php
namespace BBCStore\Catalogue\Queue\Exception;

/**
 * Thrown when the queue is empty.
 *
 * @author BBC Worldwide
 */
class EmptyQueueException extends QueueingException
{

}
