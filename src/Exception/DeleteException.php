<?php
namespace BBCStore\Catalogue\Queue\Exception;

/**
 * Thrown when there's a problemo deleting a message from the queue.
 *
 * @author BBC Worldwide
 */
class DeleteException extends QueueingException
{

}
