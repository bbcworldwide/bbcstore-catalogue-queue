<?php
namespace BBCStore\Catalogue\Queue\Exception;

/**
 * Thrown when there's a problemo reading from the queue.
 *
 * @author BBC Worldwide
 */
class ReadException extends QueueingException
{

}
