<?php
namespace BBCStore\Catalogue\Queue\Exception;

/**
 * Thrown when there's a problemo publishing a message into the queue.
 *
 * @author BBC Worldwide
 */
class PublishException extends QueueingException
{

}
