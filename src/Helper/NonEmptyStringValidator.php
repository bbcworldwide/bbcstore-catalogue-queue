<?php
namespace BBCStore\Catalogue\Queue\Helper;

/**
 * Non empty string validator
 *
 * @author BBC Worldwide
 */
class NonEmptyStringValidator
{
    /**
     * Checks whether value is a non empty string.
     *
     * @param mixed $value
     *
     * @throws Exception\ValidationError
     */
    public static function validate($value)
    {
        if ($value === null) {
            throw new Exception\ValidationError('Value is null');
        } elseif (is_string($value) === false) {
            throw new Exception\ValidationError('Value is not a string');
        } elseif (trim($value) === '') {
            throw new Exception\ValidationError('Value is empty');
        }
    }
}
