<?php
namespace BBCStore\Catalogue\Queue\Helper;

/**
 * ResourcesAndRelationships that simulates null coalescing, but without the operator.
 *
 * @author BBC Worldwide
 */
trait CoalesceTrait
{
    /**
     * Given a data structure, return the value specified at key or, if it doesn't exist, the default value.
     *
     * Similar in concept to null coalesce operator, but without the nice operator.
     *
     * Being back to PHP5 sucks.
     *
     * @param array|\stdClass $structure
     * @param string          $key
     * @param mixed           $default
     *
     * @return mixed|null
     * @throws \InvalidArgumentException
     */
    protected function coalesce($structure, $key, $default = null)
    {
        // Coalesce values if of the right type, or die.
        $value = $default;
        if ($structure instanceof \stdClass === true) {
            // Check key is good to use - only strings on stdClass
            if ($this->isValidKey($key) === false) {
                throw new \InvalidArgumentException('Invalid key');
            }

            if (property_exists($structure, $key) === true) {
                $value = $structure->$key;
            }
        } elseif (is_array($structure) === true) {
            if (array_key_exists($key, $structure) === true) {
                $value = $structure[$key];
            }
        } else {
            throw new \InvalidArgumentException('$structure must be either an array or an instance of \stdClass');
        }

        return $value;
    }

    /**
     * Ensure given key is a non empty string.
     *
     * @param string $key
     *
     * @return bool
     */
    private function isValidKey($key)
    {
        if (is_string($key)) {
            return trim($key) !== '';
        }

        return false;
    }
}
