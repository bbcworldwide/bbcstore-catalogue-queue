![BBC Store](https://store.bbc.com/sites/all/themes/barcelona/images/Logo-BBC.png)

BBC Store - Queues
==================

This is a library for the Catalogue Service apps to communicate with each other using queues. At the moment, only an AWS SQS driver is implemented.

The [messages](src/Message/Message.php) sent back and forth are specific to catalogue entities. In particular, it only contains the resource ID and the resource type to allow the receiving end of the message to zero-in on the resource on the Catalogue API.

# How to #

Head off to the [examples](examples) folder for examples on a [publisher](examples/publisher.php) and a [consumer](examples/consumer.php). Queue client is instantiated on [makeQueueClient.php](examples/makeQueueClient.php).

You can run these examples from the command line, simply copy `examples/awsCredentials.php.dist` into `examples/awsCredentials.php` and fill in your AWS creds where appropriate.

## Note

Each instance to the queue client is bound to one queue.
